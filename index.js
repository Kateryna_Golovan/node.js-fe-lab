const express = require('express')
const app = express()
const path=require('path');
const fs = require('fs');
app.use(express.json());
app.use((req, res, next)=>{
 console.log(req.method)
 console.log(req.url)
 next()
})
app.listen(8080);

fs.mkdir(path.join(__dirname, 'api/files'), { recursive: true }, (err) => {
  if (err) {
    return console.error(err);
  }
});

app.post('/api/files', (req, res) => {
    try{
        const extensions = ['.log','.txt','.json','.yaml','.xml','.js'];
        if(extensions.includes(path.extname(req.body.filename))){ 
            fs.writeFile(`./api/files/${req.body.filename}`, `${req.body.content}` , function (err) {
            if (err) return console.log(err);
            });
            res.status(200).json({
            "message": 'File created successfully'
            });
        } else { 
            return res.status(400).json({
                "message": "Wrong file extension"
            });
        } 
    } catch(err) {
        if (!req.body.filename) {
            return res.status(400).json({
                "message": "Please specify 'name' parameter"
            });
        } else if(!req.body.content) {
            return res.status(400).json({
                "message": "Please specify 'content' parameter"
            }); 
        } else {
            return res.status(500);
        } 
  }
  
})

function getFiles(req, res) {
    try {
        fs.stat('./api/files', function(err) {
            if (err) return console.log(err);
                });
                res.status(200).json({
                "message": "Success",
                "files": fs.readdirSync('./api/files/')
                });
            } catch(error){
                if (res.status(400)) {
                    return res.status(400).json({message: "Client error"});
                } else if(res.status(500)){
                    return res.status(500).json({message: 'Server error'});
                }
            }
}
app.get("/api/files", (req, res) => getFiles(req, res));

function getFile(req, res) {
    const filename = path.basename(req.url);
    const readFile = fs.readFileSync(`./api/files/${filename}`, "utf8")
    const date = fs.statSync(`./api/files/${filename}`)
    try{
        res.status(200).json({
            "message": "Success",
            "filename": filename,
            "content":  readFile,
            "extension": path.extname(req.url).slice(1),
            "uploadedDate": date.birthtime
        })
    } catch(error){
        if (res.status(400)) {
            return res.status(400).json({message: "Client error"});
        } else if(res.status(500)){
            return res.status(500).json({message: 'Server error'});
        }
    }
}
app.get(`/api/files/:filename`, (req, res) => getFile(req, res));
  


